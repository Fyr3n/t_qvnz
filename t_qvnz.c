#include "t_qvnz.h"

t_node	**init_empty_nodes(char *str)
{
	char	**connections;
	char	**names;
	t_node	**nodes = malloc(10000 * sizeof(t_node *));
	int	i;
	int	j;

	i = 0;
	j = 0;
	connections = _split(str, ' ');
	while (connections[i])
	{
		names = _split(connections[i++], '-');
		if (!node_get_by_name(nodes, _atoi(names[0])))
			nodes[j++] = node_create_named(_atoi(names[0]));
		if (!node_get_by_name(nodes, _atoi(names[1])))
			nodes[j++] = node_create_named(_atoi(names[1]));
		_split_free(names);
	}
	_split_free(connections);
	nodes[j] = 0;
	return (nodes);
}

void	connect_nodes(t_node **nodes, char *str)
{
	char	**connections;
	char	**names;
	int	i;
	t_node	*n1;
	t_node	*n2;

	i = 0;
	connections = _split(str, ' ');
	while (connections[i])
	{
		names = _split(connections[i++], '-');
		n1 = node_get_by_name(nodes, _atoi(names[0]));
	       	n2 = node_get_by_name(nodes, _atoi(names[1]));
		if (!node_are_connected(n1, n2))	
			node_connect(n1, n2);
		_split_free(names);
	}
	_split_free(connections);
}

t_vec	*find_longest_path(t_node **original_nodes, t_node **adj, t_vec *path)
{
	int	i;
	t_vec *longest_path;
	t_vec *tmp;
	t_node **connected_nodes;

	i = 0;
	longest_path = vec_cpy(path);
	while (adj[i])
	{
		if (!vec_is_in(path, adj[i]->name))
		{
			connected_nodes = node_make_list_from_adjacent(original_nodes, adj[i]->adjacent_nodes);
			tmp = find_longest_path(original_nodes, connected_nodes, vec_push_back_cpy(path, adj[i]->name));
			if (tmp->n > longest_path->n)
			{
				vec_free(longest_path);
				longest_path = tmp;
			}
			else
				vec_free(tmp);
			free(connected_nodes);
		}
		i++;
	}
	vec_free(path);
	return (longest_path);
}

void	solve_tqvnz(char *str)
{
	t_node **nodes;
	t_vec	*path;

	nodes = init_empty_nodes(str);
	connect_nodes(nodes, str);
	path = find_longest_path(nodes, nodes, vec_create());
	vec_print(path);
}
