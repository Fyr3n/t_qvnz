#include "t_qvnz.h"

t_node*	node_create()
{
	t_node* n;

       	n = malloc(sizeof(t_node));
	n->adjacent_nodes = vec_create();
	n->name = -1;
	return (n);
}

t_node* node_create_named(int name)
{
	t_node* node;

       	node = node_create();
	node->name = name;
	return (node);
}

t_node* node_get_by_name(t_node **nodes, int name)
{
	while (*nodes)
	{
		if ((*nodes)->name == name)
			return (*nodes);
		nodes++;
	}
	return (0);
}

t_node** node_make_list_from_adjacent(t_node** original_nodes, t_vec* adj)
{
	t_node	**nodes;
	int	i;

	i = 0;
	nodes = malloc((adj->n + 1) * sizeof(t_node *));
	while (i < adj->n)
	{
		nodes[i] = node_get_by_name(original_nodes, adj->data[i]);
		i++;
	}
	nodes[i] = 0;
	return (nodes);
}

int node_are_connected(t_node *n1, t_node *n2)
{
	return (vec_is_in(n1->adjacent_nodes, n2->name));
}

void node_connect(t_node *n1, t_node *n2)
{
	vec_push_back(n1->adjacent_nodes, n2->name);
	vec_push_back(n2->adjacent_nodes, n1->name);
}
