#include "t_qvnz.h"

char	**_split(char *str, char delim)
{
	char *cw;
	int cwlen;
	char **words;
	int wsz;

	cw = 0;
	cwlen = 0;
	wsz = 0;
	words = malloc(10000 * sizeof(char *));
	while (*str)
	{
		if (*str != delim && !cw)
		{
			cw = str;
			cwlen = 0;
		}
		if (*str == delim && cw)
		{
			words[wsz++] = _strndup(cw, cwlen);
			cw = 0;
		}
		cwlen++;
		str++;
	}
	if (cw)
		words[wsz++] = _strndup(cw, cwlen);
	words[wsz] = 0;
	return (words);
}

void	_split_free(char **s)
{
	int i;

	i = 0;
	while (s[i])
		free(s[i++]);
	free(s);
}
