#ifndef G_DIAM_H
# define G_DIAM_H

#include <stdlib.h>
#include <unistd.h>

typedef struct s_vec
{
	int *data;
	int n;
}	t_vec;

typedef struct s_node
{
	t_vec *adjacent_nodes;
	int name;
}	t_node;

/* memory.c */
void	_memcpy32(int *, int *, int);

/* string.c */
int	_strlen(char *);
char	*_strndup(char *, int);

/* node.c */
t_node*	node_create();
t_node* node_create_named(int name);
t_node* node_get_by_name(t_node** nodes, int name);
t_node** node_make_list_from_adjacent(t_node** original_nodes, t_vec* adjacent_nodes);
int node_are_connected(t_node *n1, t_node *n2);
void node_connect(t_node *n1, t_node *n2);

/* vector.c */
t_vec	*vec_create(void);
void	vec_push_back(t_vec *v, int new);
t_vec	*vec_push_back_cpy(t_vec *v, int new);
t_vec	*vec_cpy(t_vec *v);
int	vec_is_in(t_vec* v, int val);
void	vec_free(t_vec* v);
# include <stdio.h>
void	vec_print(t_vec* v);

/* _split.c */
char	**_split(char *str, char c);
void	_split_free(char **s);

/* _atoi.c */
int	_atoi(char *str);

#endif
