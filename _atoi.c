#include "t_qvnz.h"

int	_isspace(char c)
{
	return ((c >= 9 && c <= 13) || c == ' ');
}

int	_isnumber(char c)
{
	return (c >= '0' && c <= '9');
}

int	_atoi(char *n)
{
	unsigned int no;
	int neg;

	no = 0;
	neg = 0;
	while (_isspace(*n))
		n++;
	if (*n == '-' || *n == '+')
		if (*n++ == '-')
			neg = 1;
	while (_isnumber(*n))
		no = no * 10 + *n++ - 48;
	if (neg)
		return ((int) -no);
	return (no);
}
