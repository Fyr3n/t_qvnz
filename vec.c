#include "t_qvnz.h"

t_vec	*vec_create(void)
{
	t_vec *vec;
	
	vec = malloc(sizeof(t_vec));
	vec->data = 0;
	vec->n = 0;
	return (vec);
}

void	vec_push_back(t_vec *v, int new)
{
	int *newarr;
	
	newarr = malloc((v->n + 1) * sizeof(int));
	_memcpy32(newarr, v->data, v->n);
	if (v->data)
		free(v->data);
	newarr[v->n] = new;
	v->n++;
	v->data = newarr;
}

t_vec	*vec_push_back_cpy(t_vec *v, int new)
{
	t_vec *n;
	n = vec_create();
	
	n->data = malloc((v->n + 1) * sizeof(int));
	n->n = v->n + 1;
	_memcpy32(n->data, v->data, v->n);
	n->data[v->n] = new;
	
	return (n);
}

t_vec	*vec_cpy(t_vec *v)
{
	t_vec	*new;

	new = vec_create();
	new->data = malloc(v->n * sizeof(int));
	_memcpy32(new->data, v->data, v->n);
	new->n = v->n;
	return (new);
}

int	vec_is_in(t_vec* v, int val)
{
	int	i;

	i = 0;
	while (i < v->n)
		if (v->data[i++] == val)
			return (1);
	return (0);
}

void	vec_free(t_vec* v)
{
	free(v->data);
	free(v);
}

/* added here for visualisation purposes, in the exam i just used itoa on vec->n */
void	vec_print(t_vec* v)
{
	int i;

	i = 0;
	printf("vec->data (n=%d): [", v->n);
	while (i < v->n)
	{
		printf("%d", v->data[i]);
		if (i + 1 != v->n)
			printf(", ");
		i++;
	}
	printf("]\n");
}
