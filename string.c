#include "t_qvnz.h"

char	*_strndup(char *str, int n)
{
	char	*cpy;
	int	i;

	cpy = malloc(n + 1);
	i = 0;
	while (i < n)
	{
		cpy[i] = str[i];
		i++;
	}
	cpy[i] = 0;
	return (cpy);
}

int	_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}
